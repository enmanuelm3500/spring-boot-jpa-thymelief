package com.springboot.jpadata.app.springbootjpadata.config;

import com.springboot.jpadata.app.springbootjpadata.auth.LoginSuccesHandler;
import com.springboot.jpadata.app.springbootjpadata.service.Impl.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
@Configuration
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {


    @Autowired
    private LoginSuccesHandler loginSuccesHandler;

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    /*@Autowired
    private DataSource dataSource;*/

    @Autowired
    public void configurerGlobal(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {

        authenticationManagerBuilder.userDetailsService(userDetailsService)
                .passwordEncoder(passwordEncoder());

        /*authenticationManagerBuilder.jdbcAuthentication()
                .dataSource(dataSource)
                .passwordEncoder(passwordEncoder())
                .usersByUsernameQuery("select username, password, enabled from users where username=?")
                .authoritiesByUsernameQuery("select u.username, a.authority from authorities a inner join users u on (a.user_id=u.id) where u.username=?");*/

        /*PasswordEncoder encoder = passwordEncoder();
        User.UserBuilder userBuilder = User.builder().passwordEncoder(encoder::encode);

        authenticationManagerBuilder.inMemoryAuthentication()
                .withUser(userBuilder.username("admin".toLowerCase()).password("12345").roles("ADMIN", "USER"))
                .withUser(userBuilder.username("andres".toLowerCase()).password("12345").roles("USER"));*/
    }

    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity.authorizeRequests().antMatchers("/", "/css/**", "/js/**", "/images/**", "/api/client/**").permitAll()
                /*  .antMatchers("client/list").hasAnyRole("USER", "ADMIN")
                  .antMatchers("client/ver/**").hasAnyRole("USER", "ADMIN")
                  .antMatchers("/factura/ver/**").hasAnyRole("USER", "ADMIN")
                  .antMatchers("/uploads/**").hasAnyRole("USER")
                  .antMatchers("/form/**").hasAnyRole("ADMIN")
                  .antMatchers("/eliminar/**").hasAnyRole("ADMIN")
                  .antMatchers("/factura/**").hasAnyRole("ADMIN")*/
                .anyRequest().authenticated()
                .and()
                .formLogin()
                //.formLogin().successHandler(loginSuccesHandler)
                .loginPage("/login").permitAll()
                .and()
                .logout().permitAll()
                .and().exceptionHandling().accessDeniedPage("/error_403");

    }

}
