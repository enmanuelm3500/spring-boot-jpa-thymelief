package com.springboot.jpadata.app.springbootjpadata.repository;

import com.springboot.jpadata.app.springbootjpadata.entity.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends JpaRepository<Client, Integer> {
}
