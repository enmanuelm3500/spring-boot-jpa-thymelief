package com.springboot.jpadata.app.springbootjpadata.dao.Impl;

import com.springboot.jpadata.app.springbootjpadata.dao.ClientDao;
import com.springboot.jpadata.app.springbootjpadata.entity.Client;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Deprecated
@Repository
public class ClientDaoImpl implements ClientDao {

    @PersistenceContext
    private EntityManager entityManager;


    @Transactional(readOnly = true)
    public List<Client> findAll() {
        return entityManager.createQuery("from Client").getResultList();
    }

    @Transactional
    public void create(Client client) {
        if (client.getId() != null && client.getId() > 0) {
            entityManager.merge(client);
        } else {
            entityManager.persist(client);
        }
    }

    public Client findOne(Integer id) {
        return entityManager.find(Client.class, id);
    }

    @Transactional
    public void delete(Integer id) {
        Client client = findOne(id);
        entityManager.remove(client);
    }
}
