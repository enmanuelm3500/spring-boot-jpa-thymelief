package com.springboot.jpadata.app.springbootjpadata.service;

import com.springboot.jpadata.app.springbootjpadata.entity.Client;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface ClientService {

    List<Client> findAll();

    Page<Client> findAll(Pageable pageable);

    Client create(Client client);

    Optional<Client> findOne(Integer id);

    void delete(Integer id);

}
