package com.springboot.jpadata.app.springbootjpadata.controller;

import com.springboot.jpadata.app.springbootjpadata.entity.Client;
import com.springboot.jpadata.app.springbootjpadata.service.ClientService;
import com.springboot.jpadata.app.springbootjpadata.xml.ClientList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/client")
public class ClientRestController {

    @Autowired
    private ClientService clientService;

    @PreAuthorize("hasRole('ROLE_USER')")
    @GetMapping("/lista")
    public List<Client> getAll() {
        return clientService.findAll();
    }

}
