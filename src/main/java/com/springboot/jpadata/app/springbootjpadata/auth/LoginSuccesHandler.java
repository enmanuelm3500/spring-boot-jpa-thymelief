package com.springboot.jpadata.app.springbootjpadata.auth;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.FlashMap;
import org.springframework.web.servlet.support.SessionFlashMapManager;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@Component
public class LoginSuccesHandler extends SimpleUrlAuthenticationSuccessHandler {

    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest,
                                        HttpServletResponse httpServletResponse,
                                        Authentication authentication) throws IOException, ServletException {

        SessionFlashMapManager sessionFlashMapManager = new SessionFlashMapManager();

        FlashMap flashMap = new FlashMap();

        flashMap.addTargetRequestParam("info", "HA INICIADO SESSION CON EXITO");

        sessionFlashMapManager.saveOutputFlashMap(flashMap, httpServletRequest, httpServletResponse);

        super.onAuthenticationSuccess(httpServletRequest, httpServletResponse, authentication);
    }


}
