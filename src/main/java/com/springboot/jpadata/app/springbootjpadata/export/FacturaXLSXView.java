package com.springboot.jpadata.app.springbootjpadata.export;

import com.springboot.jpadata.app.springbootjpadata.entity.Factura;
import com.springboot.jpadata.app.springbootjpadata.entity.ItemFactura;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.document.AbstractXlsxView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@Component("factura/ver.xlsx")
public class FacturaXLSXView extends AbstractXlsxView {

    @Override
    protected void buildExcelDocument(Map<String, Object> map, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Factura factura = (Factura) map.get("factura");

        Sheet sheet = workbook.createSheet();
        MessageSourceAccessor mensajes = getMessageSourceAccessor();

        sheet.createRow(1).createCell(0).setCellValue("Datos del Cliente: " + factura.getClient().getName() + " " + factura.getClient().getAddres());
        sheet.createRow(2).createCell(0).setCellValue("Email: " + factura.getClient().getEmail());
        sheet.createRow(4).createCell(0).setCellValue("Datos de la factura");
        sheet.createRow(5).createCell(0).setCellValue("Folio: " + factura.getId());
        sheet.createRow(6).createCell(0).setCellValue("Descripcion: ".concat(factura.getDescription()));
        sheet.createRow(7).createCell(0).setCellValue("Fecha: ".concat(factura.getCreateDate().toString()));

        Row row = sheet.createRow(9);
        row.createCell(0).setCellValue("producto");
        row.createCell(1).setCellValue("precio");
        row.createCell(2).setCellValue("cantidad");
        row.createCell(3).setCellValue("total");
        int rowNum = 10;
        for (ItemFactura itemFactura : factura.getItemFacturaList()) {

            Row cells = sheet.createRow(rowNum++);
            cells.createCell(0).setCellValue(itemFactura.getProducts().getNombre());
            cells.createCell(1).setCellValue(itemFactura.getProducts().getPrice().toString());
            cells.createCell(2).setCellValue(itemFactura.getCantidad().toString());
            cells.createCell(3).setCellValue(itemFactura.calcularItem());
        }
        sheet.createRow(rowNum).createCell(2).setCellValue("Total: ");
        sheet.createRow(rowNum).createCell(3).setCellValue(factura.getTotal().toString());
    }
}
