package com.springboot.jpadata.app.springbootjpadata.service;

import com.springboot.jpadata.app.springbootjpadata.entity.Product;

import java.util.List;
import java.util.Optional;

public interface ProductService {

    List<Product> findByNombreIsLike(String item);

    Optional<Product> findById(Long id);


}
