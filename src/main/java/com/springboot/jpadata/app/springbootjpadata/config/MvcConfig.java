package com.springboot.jpadata.app.springbootjpadata.config;

import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

public class MvcConfig implements WebMvcConfigurer {


    private void addViewController(ViewControllerRegistry registry){
        registry.addViewController("/error_403").setViewName("error_403");
    }
}
