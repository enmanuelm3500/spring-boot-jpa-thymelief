package com.springboot.jpadata.app.springbootjpadata.repository;

import com.springboot.jpadata.app.springbootjpadata.entity.Usuario;
import org.springframework.data.repository.CrudRepository;

public interface UsuarioRepository extends CrudRepository<Usuario, Long> {
    Usuario findByUsername(String name);
}
