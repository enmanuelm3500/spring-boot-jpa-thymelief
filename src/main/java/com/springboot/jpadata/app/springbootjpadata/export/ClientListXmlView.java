package com.springboot.jpadata.app.springbootjpadata.export;

import com.springboot.jpadata.app.springbootjpadata.entity.Client;
import com.springboot.jpadata.app.springbootjpadata.xml.ClientList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.xml.MarshallingView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@Component("/listar.xml")
public class ClientListXmlView extends MarshallingView {


    @Autowired
    public ClientListXmlView(Jaxb2Marshaller marshall){
        super(marshall);
    }

    @Override
    protected void renderMergedOutputModel(Map<String, Object> model,
                                           HttpServletRequest request,
                                           HttpServletResponse response) throws Exception {

        model.remove("titulo");
        model.remove("page");
        Page<Client> clients = (Page<Client>) model.get("clientes");
        model.remove("clientes");
        model.put("clienteList", new ClientList(clients.getContent()));


        super.renderMergedOutputModel(model, request, response);
    }
}
