package com.springboot.jpadata.app.springbootjpadata.service;

import com.springboot.jpadata.app.springbootjpadata.entity.Factura;

public interface FacturaServise {

    Factura saveFactura(Factura factura);

    Factura findById(Long id);

    String delete(Long id);
}
