package com.springboot.jpadata.app.springbootjpadata.controller;


import com.springboot.jpadata.app.springbootjpadata.entity.Client;
import com.springboot.jpadata.app.springbootjpadata.entity.Factura;
import com.springboot.jpadata.app.springbootjpadata.entity.ItemFactura;
import com.springboot.jpadata.app.springbootjpadata.entity.Product;
import com.springboot.jpadata.app.springbootjpadata.service.ClientService;
import com.springboot.jpadata.app.springbootjpadata.service.FacturaServise;
import com.springboot.jpadata.app.springbootjpadata.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.Optional;

//@Secured("ROLE_ADMIN")
@Controller
@RequestMapping("/factura")
public class FacturaController {

    private static final String DEFAUL_TITLE = "titulo";
    private static final String DEFAUL_DANGER = "danger";
    private static final String DEFAUL_ERROR = "error";
    private static final String DEFAUL_CLIENT = "client";
    private static final String REDIRECT_TO_LIST = "redirect:../../client/list";

    @Autowired
    private ClientService clientService;

    @Autowired
    private ProductService productService;

    @Autowired
    private FacturaServise facturaServise;

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping("/crear/{idClient}")
    public String createFactura(@PathVariable Integer idClient,
                                RedirectAttributes flash,
                                Map<String, Object> model) {
        Optional<Client> client = clientService.findOne(idClient);
        if (!client.isPresent()) {
            flash.addFlashAttribute(DEFAUL_DANGER, "no se enecontro cliente en BD");
            return REDIRECT_TO_LIST;
        }
        Factura factura = new Factura();
        factura.setClient(client.get());
        model.put("factura", factura);
        model.put(DEFAUL_TITLE, "Crear Factura");
        return "factura/formulario";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping(value = "/cargar-productos/{term}", produces = {"application/json"})
    public @ResponseBody
    List<Product> cargarProductos(@PathVariable String term) {
        return productService.findByNombreIsLike(term);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping("/form")
    public String createFactura(@Valid Factura factura,
                                BindingResult bindingResult,
                                Model model,
                                @RequestParam(name = "item_id[]", required = false) Long[] itemId,
                                @RequestParam(name = "cantidad[]", required = false) Integer[] cantidad,
                                @RequestParam(name = "client", required = false) Integer clientId,
                                RedirectAttributes flash,
                                SessionStatus status) {
        // factura.setCreateDate(new Date());
        if (bindingResult.hasErrors()) {
            model.addAttribute("titulo", "crear factura");
            return "redirect:../../factura/crear/" + clientId;
        }

        for (int i = 0; i < itemId.length; i++) {
            Optional<Product> product = productService.findById(itemId[i]);
            ItemFactura itemFactura = new ItemFactura();
            if (product.isPresent()) {
                itemFactura.setProducts(product.get());
            }
            itemFactura.setCantidad(cantidad[i]);
            itemFactura.setProducts(product.get());

            factura.addItem(itemFactura);
        }
        facturaServise.saveFactura(factura);
        status.setComplete();
        flash.addFlashAttribute("success", "Factura creada con exito");
        return "redirect:../../client/list";
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @GetMapping("/ver/{id}")
    public String verFactura(@PathVariable Long id,
                             RedirectAttributes flash,
                             Model model) {
        Factura factura = facturaServise.findById(id);
        if (factura == null) {
            flash.addFlashAttribute(DEFAUL_ERROR, "no se encontro la factura");
            return REDIRECT_TO_LIST;
        }
        model.addAttribute("factura", factura);
        model.addAttribute(DEFAUL_TITLE, "Factura:".concat(factura.getDescription()));
        return "factura/ver";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping("/eliminar/{id}")
    public String deleteFactura(@PathVariable Long id,RedirectAttributes flash) {
        flash.addFlashAttribute("info", facturaServise.delete(id));
        return REDIRECT_TO_LIST;
    }

}
