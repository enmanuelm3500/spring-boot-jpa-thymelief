package com.springboot.jpadata.app.springbootjpadata.export;

import com.lowagie.text.Document;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.*;
import com.springboot.jpadata.app.springbootjpadata.entity.Factura;
import com.springboot.jpadata.app.springbootjpadata.entity.ItemFactura;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.document.AbstractPdfView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@Component("factura/ver")
public class FacturaPDFView extends AbstractPdfView {
    @Override
    protected void buildPdfDocument(Map<String, Object> map, Document document, PdfWriter pdfWriter, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {

        Factura factura = (Factura) map.get("factura");

        PdfPTable pdfHeader = new PdfPTable(1);
        pdfHeader.setSpacingAfter(20);
        pdfHeader.addCell("Datos del Cliente");
        pdfHeader.addCell("Nombre: " + factura.getClient().getName());
        pdfHeader.addCell("Apellido: " + factura.getClient().getAddres());

        PdfPTable pdfInfoGeneral = new PdfPTable(1);
        pdfInfoGeneral.setSpacingAfter(20);
        pdfInfoGeneral.addCell("Datos de la factura");
        pdfInfoGeneral.addCell("Folio: " + factura.getId());
        pdfInfoGeneral.addCell("Fecha de Creacion: " + factura.getCreateDate());
        pdfInfoGeneral.addCell("Descripción: " + factura.getDescription());
        pdfInfoGeneral.addCell("Observación: " + factura.getObservaciones());

        PdfPTable pdfBody = new PdfPTable(4);
        pdfBody.setSpacingAfter(20);
        pdfBody.addCell("Producto: ");
        pdfBody.addCell("Precio: " );
        pdfBody.addCell("Cantidad: " );
        pdfBody.addCell("Total: " );

        for(ItemFactura itemFactura : factura.getItemFacturaList()){
            pdfBody.addCell(itemFactura.getProducts().getNombre());
            pdfBody.addCell(itemFactura.getProducts().getPrice().toString());
            pdfBody.addCell(itemFactura.getCantidad().toString());
            pdfBody.addCell(itemFactura.calcularItem().toString());
        }

        PdfPCell pdfPCell = new PdfPCell( new Phrase("Total: "));
        pdfPCell.setColspan(3);
        pdfPCell.setHorizontalAlignment(PdfCell.ALIGN_RIGHT);
        pdfBody.addCell(pdfPCell);
        pdfBody.addCell(factura.getTotal().toString());

        document.add(pdfHeader);
        document.add(pdfInfoGeneral);
        document.add(pdfBody);


    }
}
