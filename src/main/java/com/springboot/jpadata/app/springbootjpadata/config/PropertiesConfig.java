package com.springboot.jpadata.app.springbootjpadata.config;


import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@Configuration
@PropertySources(
        @PropertySource("classpath:factura.properties")
)
public class PropertiesConfig {
}
