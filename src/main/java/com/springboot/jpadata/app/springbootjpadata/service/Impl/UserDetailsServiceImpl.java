package com.springboot.jpadata.app.springbootjpadata.service.Impl;

import com.springboot.jpadata.app.springbootjpadata.entity.Role;
import com.springboot.jpadata.app.springbootjpadata.entity.Usuario;
import com.springboot.jpadata.app.springbootjpadata.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Transactional(readOnly = true)
    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {

        Usuario usuario = usuarioRepository.findByUsername(s);

        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        /*usuario.getRoles()
                .stream()
                .map(role -> {
                    grantedAuthorities.add(new SimpleGrantedAuthority(role.getAuthority()));
                    return null;
                })
                .collect(Collectors.toList());*/
        for(Role role : usuario.getRoles()){
            grantedAuthorities.add(new SimpleGrantedAuthority(role.getAuthority()));
        }

        return new User(usuario.getUsername(),usuario.getPassword(),usuario.getEnabled(),true,true,true, grantedAuthorities);
    }
}
