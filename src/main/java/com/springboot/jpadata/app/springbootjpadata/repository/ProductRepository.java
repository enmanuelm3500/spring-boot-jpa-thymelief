package com.springboot.jpadata.app.springbootjpadata.repository;

import com.springboot.jpadata.app.springbootjpadata.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface ProductRepository extends CrudRepository<Product, Long> {

    @Query("select p from Product p where p.nombre like %?1%")
    List<Product> findByNombre(String term);

    List<Product> findByNombreLikeIgnoreCase(String term);

    Optional<Product> findById(Long id);

   // List<Product> findByNombreLikeIgnoreCase(String item);

}
