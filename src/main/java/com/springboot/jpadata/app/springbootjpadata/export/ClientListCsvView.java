package com.springboot.jpadata.app.springbootjpadata.export;

import com.springboot.jpadata.app.springbootjpadata.entity.Client;
import com.springboot.jpadata.app.springbootjpadata.xml.ClientList;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.AbstractView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

@Component("/listar.csv")
//@Component("/cliente/listar.csv")
public class ClientListCsvView extends AbstractView {


    public ClientListCsvView() {
        setContentType("text/csv");
    }

    @Override
    protected boolean generatesDownloadContent() {
        return true;
    }

    @Override
    protected void renderMergedOutputModel(Map<String, Object> map,
                                           HttpServletRequest httpServletRequest,
                                           HttpServletResponse httpServletResponse) throws Exception {

        httpServletResponse.setHeader("Content-Disposition", "attachment; filename=\"clientes.csv\"");
        httpServletResponse.setContentType(getContentType());


        Page<Client> clientes = (Page<Client>) map.get("clientes");

        ICsvBeanWriter beanWriter = new CsvBeanWriter(httpServletResponse.getWriter(),  CsvPreference.STANDARD_PREFERENCE);

        String[] header = {"id", "name", "addres", "email", "created"};
        beanWriter.writeHeader(header);

        for(Client cliente: clientes) {
           beanWriter.write(cliente, header);
        }

        beanWriter.close();
    }
}
