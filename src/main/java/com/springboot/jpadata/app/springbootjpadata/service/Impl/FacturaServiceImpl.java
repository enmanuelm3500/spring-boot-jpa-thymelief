package com.springboot.jpadata.app.springbootjpadata.service.Impl;

import com.springboot.jpadata.app.springbootjpadata.entity.Client;
import com.springboot.jpadata.app.springbootjpadata.entity.Factura;
import com.springboot.jpadata.app.springbootjpadata.repository.FacturaRepository;
import com.springboot.jpadata.app.springbootjpadata.service.FacturaServise;
import net.bytebuddy.asm.Advice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.MessagingException;
import java.util.List;
import java.util.Optional;


@Service
public class FacturaServiceImpl implements FacturaServise {

    @Autowired
    private MailService mailService;

    @Autowired
    private FacturaRepository facturaRepository;

    @Override
    public Factura saveFactura(Factura factura) {
        mailService.sendEmail("enmanuelm3500@gmail.com","factura","prueba");
        return facturaRepository.save(factura);
    }

    /*public void sendEmail(List<Client> listClients, String subject, String content) {

        for (Client c : listClients)
        {
            SimpleMailMessage email = new SimpleMailMessage();

            //recorremos la lista y enviamos a cada cliente el mismo correo
            email.setTo(c.getEmail());
            email.setSubject(subject);
            email.setText(content);

            mailService.sendEmail("enmanuelm3500@gmail.com","factura","prueba");
        }
    }*/

    @Override
    @Transactional(readOnly = true)
    public Factura findById(Long id) {
        return facturaRepository.findById(id).orElse(null);
    }

    @Override
    public String delete(Long id) {
        if(facturaRepository.findById(id).isPresent()){
             facturaRepository.deleteById(id);
            return "Factura Eliminada con exito";
        }else {
            return "No se encontraron registros para eliminar";
        }
    }
}
